<?xml version = "1.0" encoding="UTF-8" standalone="yes"?>
<CPLEXSolution version="1.2">
 <header
   problemName="TP1"
   solutionName="incumbent"
   solutionIndex="-1"
   objectiveValue="18"
   solutionTypeValue="3"
   solutionTypeString="primal"
   solutionStatusValue="101"
   solutionStatusString="integer optimal solution"
   solutionMethodString="mip"
   primalFeasible="1"
   dualFeasible="1"
   MIPNodes="0"
   MIPIterations="35"
   writeLevel="1"/>
 <quality
   epInt="1.0000000000000001e-05"
   epRHS="9.9999999999999995e-07"
   maxIntInfeas="0"
   maxPrimalInfeas="0"
   maxX="1"
   maxSlack="393"/>
 <linearConstraints>
  <constraint name="capacity_bin_i0" index="0" slack="27"/>
  <constraint name="capacity_bin_i1" index="1" slack="22"/>
  <constraint name="capacity_bin_i2" index="2" slack="393"/>
  <constraint name="capacity_bin_i3" index="3" slack="0"/>
  <constraint name="capacity_bin_i4" index="4" slack="0"/>
  <constraint name="capacity_bin_i5" index="5" slack="0"/>
  <constraint name="capacity_bin_i6" index="6" slack="0"/>
  <constraint name="capacity_bin_i7" index="7" slack="0"/>
  <constraint name="capacity_bin_i8" index="8" slack="0"/>
  <constraint name="capacity_bin_i9" index="9" slack="0"/>
  <constraint name="capacity_bin_i10" index="10" slack="0"/>
  <constraint name="capacity_bin_i11" index="11" slack="0"/>
  <constraint name="capacity_bin_i12" index="12" slack="0"/>
  <constraint name="capacity_bin_i13" index="13" slack="0"/>
  <constraint name="capacity_bin_i14" index="14" slack="0"/>
  <constraint name="number of bin used0" index="15" slack="0"/>
  <constraint name="number of bin used1" index="16" slack="0"/>
  <constraint name="number of bin used2" index="17" slack="0"/>
  <constraint name="number of bin used3" index="18" slack="0"/>
  <constraint name="number of bin used4" index="19" slack="0"/>
  <constraint name="number of bin used5" index="20" slack="0"/>
  <constraint name="number of bin used6" index="21" slack="0"/>
  <constraint name="number of bin used7" index="22" slack="0"/>
  <constraint name="number of bin used8" index="23" slack="0"/>
  <constraint name="number of bin used9" index="24" slack="0"/>
  <constraint name="number of bin used10" index="25" slack="0"/>
  <constraint name="number of bin used11" index="26" slack="0"/>
  <constraint name="number of bin used12" index="27" slack="0"/>
  <constraint name="number of bin used13" index="28" slack="0"/>
  <constraint name="number of bin used14" index="29" slack="0"/>
 </linearConstraints>
 <variables>
  <variable name="y_i0" index="0" value="1"/>
  <variable name="y_i1" index="1" value="1"/>
  <variable name="y_i2" index="2" value="1"/>
  <variable name="y_i3" index="3" value="0"/>
  <variable name="y_i4" index="4" value="0"/>
  <variable name="y_i5" index="5" value="0"/>
  <variable name="y_i6" index="6" value="0"/>
  <variable name="y_i7" index="7" value="0"/>
  <variable name="y_i8" index="8" value="0"/>
  <variable name="y_i9" index="9" value="0"/>
  <variable name="y_i10" index="10" value="0"/>
  <variable name="y_i11" index="11" value="0"/>
  <variable name="y_i12" index="12" value="0"/>
  <variable name="y_i13" index="13" value="0"/>
  <variable name="y_i14" index="14" value="0"/>
  <variable name="x_i0j0" index="15" value="1"/>
  <variable name="x_i0j1" index="16" value="1"/>
  <variable name="x_i0j2" index="17" value="1"/>
  <variable name="x_i0j3" index="18" value="1"/>
  <variable name="x_i0j4" index="19" value="1"/>
  <variable name="x_i0j5" index="20" value="1"/>
  <variable name="x_i0j6" index="21" value="0"/>
  <variable name="x_i0j7" index="22" value="1"/>
  <variable name="x_i0j8" index="23" value="0"/>
  <variable name="x_i0j9" index="24" value="0"/>
  <variable name="x_i0j10" index="25" value="0"/>
  <variable name="x_i0j11" index="26" value="0"/>
  <variable name="x_i0j12" index="27" value="0"/>
  <variable name="x_i0j13" index="28" value="0"/>
  <variable name="x_i0j14" index="29" value="0"/>
  <variable name="x_i1j0" index="30" value="0"/>
  <variable name="x_i1j1" index="31" value="0"/>
  <variable name="x_i1j2" index="32" value="0"/>
  <variable name="x_i1j3" index="33" value="0"/>
  <variable name="x_i1j4" index="34" value="0"/>
  <variable name="x_i1j5" index="35" value="0"/>
  <variable name="x_i1j6" index="36" value="1"/>
  <variable name="x_i1j7" index="37" value="0"/>
  <variable name="x_i1j8" index="38" value="1"/>
  <variable name="x_i1j9" index="39" value="1"/>
  <variable name="x_i1j10" index="40" value="1"/>
  <variable name="x_i1j11" index="41" value="1"/>
  <variable name="x_i1j12" index="42" value="0"/>
  <variable name="x_i1j13" index="43" value="1"/>
  <variable name="x_i1j14" index="44" value="0"/>
  <variable name="x_i2j0" index="45" value="0"/>
  <variable name="x_i2j1" index="46" value="0"/>
  <variable name="x_i2j2" index="47" value="0"/>
  <variable name="x_i2j3" index="48" value="0"/>
  <variable name="x_i2j4" index="49" value="0"/>
  <variable name="x_i2j5" index="50" value="0"/>
  <variable name="x_i2j6" index="51" value="0"/>
  <variable name="x_i2j7" index="52" value="0"/>
  <variable name="x_i2j8" index="53" value="0"/>
  <variable name="x_i2j9" index="54" value="0"/>
  <variable name="x_i2j10" index="55" value="0"/>
  <variable name="x_i2j11" index="56" value="0"/>
  <variable name="x_i2j12" index="57" value="1"/>
  <variable name="x_i2j13" index="58" value="0"/>
  <variable name="x_i2j14" index="59" value="1"/>
  <variable name="x_i3j0" index="60" value="0"/>
  <variable name="x_i3j1" index="61" value="0"/>
  <variable name="x_i3j2" index="62" value="0"/>
  <variable name="x_i3j3" index="63" value="0"/>
  <variable name="x_i3j4" index="64" value="0"/>
  <variable name="x_i3j5" index="65" value="0"/>
  <variable name="x_i3j6" index="66" value="0"/>
  <variable name="x_i3j7" index="67" value="0"/>
  <variable name="x_i3j8" index="68" value="0"/>
  <variable name="x_i3j9" index="69" value="0"/>
  <variable name="x_i3j10" index="70" value="0"/>
  <variable name="x_i3j11" index="71" value="0"/>
  <variable name="x_i3j12" index="72" value="0"/>
  <variable name="x_i3j13" index="73" value="0"/>
  <variable name="x_i3j14" index="74" value="0"/>
  <variable name="x_i4j0" index="75" value="0"/>
  <variable name="x_i4j1" index="76" value="0"/>
  <variable name="x_i4j2" index="77" value="0"/>
  <variable name="x_i4j3" index="78" value="0"/>
  <variable name="x_i4j4" index="79" value="0"/>
  <variable name="x_i4j5" index="80" value="0"/>
  <variable name="x_i4j6" index="81" value="0"/>
  <variable name="x_i4j7" index="82" value="0"/>
  <variable name="x_i4j8" index="83" value="0"/>
  <variable name="x_i4j9" index="84" value="0"/>
  <variable name="x_i4j10" index="85" value="0"/>
  <variable name="x_i4j11" index="86" value="0"/>
  <variable name="x_i4j12" index="87" value="0"/>
  <variable name="x_i4j13" index="88" value="0"/>
  <variable name="x_i4j14" index="89" value="0"/>
  <variable name="x_i5j0" index="90" value="0"/>
  <variable name="x_i5j1" index="91" value="0"/>
  <variable name="x_i5j2" index="92" value="0"/>
  <variable name="x_i5j3" index="93" value="0"/>
  <variable name="x_i5j4" index="94" value="0"/>
  <variable name="x_i5j5" index="95" value="0"/>
  <variable name="x_i5j6" index="96" value="0"/>
  <variable name="x_i5j7" index="97" value="0"/>
  <variable name="x_i5j8" index="98" value="0"/>
  <variable name="x_i5j9" index="99" value="0"/>
  <variable name="x_i5j10" index="100" value="0"/>
  <variable name="x_i5j11" index="101" value="0"/>
  <variable name="x_i5j12" index="102" value="0"/>
  <variable name="x_i5j13" index="103" value="0"/>
  <variable name="x_i5j14" index="104" value="0"/>
  <variable name="x_i6j0" index="105" value="0"/>
  <variable name="x_i6j1" index="106" value="0"/>
  <variable name="x_i6j2" index="107" value="0"/>
  <variable name="x_i6j3" index="108" value="0"/>
  <variable name="x_i6j4" index="109" value="0"/>
  <variable name="x_i6j5" index="110" value="0"/>
  <variable name="x_i6j6" index="111" value="0"/>
  <variable name="x_i6j7" index="112" value="0"/>
  <variable name="x_i6j8" index="113" value="0"/>
  <variable name="x_i6j9" index="114" value="0"/>
  <variable name="x_i6j10" index="115" value="0"/>
  <variable name="x_i6j11" index="116" value="0"/>
  <variable name="x_i6j12" index="117" value="0"/>
  <variable name="x_i6j13" index="118" value="0"/>
  <variable name="x_i6j14" index="119" value="0"/>
  <variable name="x_i7j0" index="120" value="0"/>
  <variable name="x_i7j1" index="121" value="0"/>
  <variable name="x_i7j2" index="122" value="0"/>
  <variable name="x_i7j3" index="123" value="0"/>
  <variable name="x_i7j4" index="124" value="0"/>
  <variable name="x_i7j5" index="125" value="0"/>
  <variable name="x_i7j6" index="126" value="0"/>
  <variable name="x_i7j7" index="127" value="0"/>
  <variable name="x_i7j8" index="128" value="0"/>
  <variable name="x_i7j9" index="129" value="0"/>
  <variable name="x_i7j10" index="130" value="0"/>
  <variable name="x_i7j11" index="131" value="0"/>
  <variable name="x_i7j12" index="132" value="0"/>
  <variable name="x_i7j13" index="133" value="0"/>
  <variable name="x_i7j14" index="134" value="0"/>
  <variable name="x_i8j0" index="135" value="0"/>
  <variable name="x_i8j1" index="136" value="0"/>
  <variable name="x_i8j2" index="137" value="0"/>
  <variable name="x_i8j3" index="138" value="0"/>
  <variable name="x_i8j4" index="139" value="0"/>
  <variable name="x_i8j5" index="140" value="0"/>
  <variable name="x_i8j6" index="141" value="0"/>
  <variable name="x_i8j7" index="142" value="0"/>
  <variable name="x_i8j8" index="143" value="0"/>
  <variable name="x_i8j9" index="144" value="0"/>
  <variable name="x_i8j10" index="145" value="0"/>
  <variable name="x_i8j11" index="146" value="0"/>
  <variable name="x_i8j12" index="147" value="0"/>
  <variable name="x_i8j13" index="148" value="0"/>
  <variable name="x_i8j14" index="149" value="0"/>
  <variable name="x_i9j0" index="150" value="0"/>
  <variable name="x_i9j1" index="151" value="0"/>
  <variable name="x_i9j2" index="152" value="0"/>
  <variable name="x_i9j3" index="153" value="0"/>
  <variable name="x_i9j4" index="154" value="0"/>
  <variable name="x_i9j5" index="155" value="0"/>
  <variable name="x_i9j6" index="156" value="0"/>
  <variable name="x_i9j7" index="157" value="0"/>
  <variable name="x_i9j8" index="158" value="0"/>
  <variable name="x_i9j9" index="159" value="0"/>
  <variable name="x_i9j10" index="160" value="0"/>
  <variable name="x_i9j11" index="161" value="0"/>
  <variable name="x_i9j12" index="162" value="0"/>
  <variable name="x_i9j13" index="163" value="0"/>
  <variable name="x_i9j14" index="164" value="0"/>
  <variable name="x_i10j0" index="165" value="0"/>
  <variable name="x_i10j1" index="166" value="0"/>
  <variable name="x_i10j2" index="167" value="0"/>
  <variable name="x_i10j3" index="168" value="0"/>
  <variable name="x_i10j4" index="169" value="0"/>
  <variable name="x_i10j5" index="170" value="0"/>
  <variable name="x_i10j6" index="171" value="0"/>
  <variable name="x_i10j7" index="172" value="0"/>
  <variable name="x_i10j8" index="173" value="0"/>
  <variable name="x_i10j9" index="174" value="0"/>
  <variable name="x_i10j10" index="175" value="0"/>
  <variable name="x_i10j11" index="176" value="0"/>
  <variable name="x_i10j12" index="177" value="0"/>
  <variable name="x_i10j13" index="178" value="0"/>
  <variable name="x_i10j14" index="179" value="0"/>
  <variable name="x_i11j0" index="180" value="0"/>
  <variable name="x_i11j1" index="181" value="0"/>
  <variable name="x_i11j2" index="182" value="0"/>
  <variable name="x_i11j3" index="183" value="0"/>
  <variable name="x_i11j4" index="184" value="0"/>
  <variable name="x_i11j5" index="185" value="0"/>
  <variable name="x_i11j6" index="186" value="0"/>
  <variable name="x_i11j7" index="187" value="0"/>
  <variable name="x_i11j8" index="188" value="0"/>
  <variable name="x_i11j9" index="189" value="0"/>
  <variable name="x_i11j10" index="190" value="0"/>
  <variable name="x_i11j11" index="191" value="0"/>
  <variable name="x_i11j12" index="192" value="0"/>
  <variable name="x_i11j13" index="193" value="0"/>
  <variable name="x_i11j14" index="194" value="0"/>
  <variable name="x_i12j0" index="195" value="0"/>
  <variable name="x_i12j1" index="196" value="0"/>
  <variable name="x_i12j2" index="197" value="0"/>
  <variable name="x_i12j3" index="198" value="0"/>
  <variable name="x_i12j4" index="199" value="0"/>
  <variable name="x_i12j5" index="200" value="0"/>
  <variable name="x_i12j6" index="201" value="0"/>
  <variable name="x_i12j7" index="202" value="0"/>
  <variable name="x_i12j8" index="203" value="0"/>
  <variable name="x_i12j9" index="204" value="0"/>
  <variable name="x_i12j10" index="205" value="0"/>
  <variable name="x_i12j11" index="206" value="0"/>
  <variable name="x_i12j12" index="207" value="0"/>
  <variable name="x_i12j13" index="208" value="0"/>
  <variable name="x_i12j14" index="209" value="0"/>
  <variable name="x_i13j0" index="210" value="0"/>
  <variable name="x_i13j1" index="211" value="0"/>
  <variable name="x_i13j2" index="212" value="0"/>
  <variable name="x_i13j3" index="213" value="0"/>
  <variable name="x_i13j4" index="214" value="0"/>
  <variable name="x_i13j5" index="215" value="0"/>
  <variable name="x_i13j6" index="216" value="0"/>
  <variable name="x_i13j7" index="217" value="0"/>
  <variable name="x_i13j8" index="218" value="0"/>
  <variable name="x_i13j9" index="219" value="0"/>
  <variable name="x_i13j10" index="220" value="0"/>
  <variable name="x_i13j11" index="221" value="0"/>
  <variable name="x_i13j12" index="222" value="0"/>
  <variable name="x_i13j13" index="223" value="0"/>
  <variable name="x_i13j14" index="224" value="0"/>
  <variable name="x_i14j0" index="225" value="0"/>
  <variable name="x_i14j1" index="226" value="0"/>
  <variable name="x_i14j2" index="227" value="0"/>
  <variable name="x_i14j3" index="228" value="0"/>
  <variable name="x_i14j4" index="229" value="0"/>
  <variable name="x_i14j5" index="230" value="0"/>
  <variable name="x_i14j6" index="231" value="0"/>
  <variable name="x_i14j7" index="232" value="0"/>
  <variable name="x_i14j8" index="233" value="0"/>
  <variable name="x_i14j9" index="234" value="0"/>
  <variable name="x_i14j10" index="235" value="0"/>
  <variable name="x_i14j11" index="236" value="0"/>
  <variable name="x_i14j12" index="237" value="0"/>
  <variable name="x_i14j13" index="238" value="0"/>
  <variable name="x_i14j14" index="239" value="0"/>
 </variables>
</CPLEXSolution>
