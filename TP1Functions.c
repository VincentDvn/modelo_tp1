#include "TP1Functions.h"
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/time.h>
#include <stdio.h>	
#include <ilcplex/cplex.h>

int read_TP1_instance(FILE*fin,dataSet* dsptr)
{
	int rval = 0;

	//Taille des boites
	int V;
	//Nombre d'objets
	int n;
	fscanf(fin,"%d,%d\n",&n,&V);
	dsptr->V = V;
	dsptr->n = n;
	dsptr->size = (int*)malloc(sizeof(int)*n);

	int i;
	for( i = 0 ; i < n ; i++)
		fscanf(fin,"%d\n",&(dsptr->size[i]));

	fprintf(stderr,"Instance file read, each bin is %d long and there is %d items of lengths:\n",
			V,n);
	for( i = 0 ; i < n ; i++)
		fprintf(stderr,"%d\t",dsptr->size[i]);
	fprintf(stderr,"\n");


	return rval;
}

int TP1_solve_exact(dataSet* dsptr)
{
	int rval = 0;

	IP_problem* ip_prob_ptr = &(dsptr->master);
	ip_prob_ptr->env = NULL;
	ip_prob_ptr->lp = NULL;
	ip_prob_ptr->env = CPXopenCPLEX (&rval);
	if(rval) fprintf(stderr,"ERROR WHILE CALLING CPXopenCPLEX\n");
	if ( ip_prob_ptr->env == NULL ) 
	{
		char  errmsg[1024];
		fprintf (stderr, "Could not open CPLEX environment.\n");
		CPXgeterrorstring (ip_prob_ptr->env, rval, errmsg);
		fprintf (stderr, "%s", errmsg);
		exit(0);	
	}

	//We create the MIP problem
	ip_prob_ptr->lp = CPXcreateprob (ip_prob_ptr->env, &rval, "TP1");
	if(rval) fprintf(stderr,"ERROR WHILE CALLING CPXcreateprob\n");

	rval = CPXsetintparam (ip_prob_ptr->env, CPX_PARAM_DATACHECK, CPX_ON); 
	rval = CPXsetintparam (ip_prob_ptr->env, CPX_PARAM_SCRIND, CPX_ON);

	int n = dsptr->n;
	int*size = dsptr->size;
	int V = dsptr->V;
	int nv = n+ n*n;

	//We fill our arrays
	//Memory
	ip_prob_ptr->nv = nv;
	ip_prob_ptr->x = (double*)malloc(sizeof(double)*nv);
	ip_prob_ptr->cost = (double*)malloc(sizeof(double)*nv);
	ip_prob_ptr->c_type = (char*)malloc(sizeof(char)*nv);
	ip_prob_ptr->up_bound = (double*)malloc(sizeof(double)*nv);
	ip_prob_ptr->low_bound = (double*)malloc(sizeof(double)*nv);
	ip_prob_ptr->var_name = (char**)malloc(sizeof(char*)*nv);

	int i,j,id = 0;
	//Structures keeping the index of each variable
	int*id_y_i = (int*)malloc(sizeof(int)*n);
	int**id_x_ij = (int**)malloc(sizeof(int*)*n);
	for( i = 0 ; i < n ; i++)
		id_x_ij[i] = (int*)malloc(sizeof(int)*n);

	//First the variables yi (bin #i used or not)
	for( i = 0 ; i < n ; i++) 
	{
		//We keep the id
		id_y_i[i] = id;

		//We generate the variable attributes
		ip_prob_ptr->x[id] = 0;
		ip_prob_ptr->cost[id] = 1;
		ip_prob_ptr->c_type[id] = 'B';
		ip_prob_ptr->up_bound[id] = 1;
		ip_prob_ptr->low_bound[id] = 0;
		ip_prob_ptr->var_name[id] = (char*)malloc(sizeof(char)*1024);
	        snprintf(       ip_prob_ptr->var_name[id],
        	                1024,
                	        "y_i%d",
                        	i);
		id++;
	}


	/********************************************/
	/******* FILL HERE FOR VARIABLES x_ij *******/
	/********************************************/

	for( i = 0 ; i < n ; i++) 
	{
		for( j = 0 ; j < n ; j++) 
		{
			id_x_ij[i][j]  = id;

			ip_prob_ptr->x[id] = 0;
			ip_prob_ptr->cost[id] = 1;
			ip_prob_ptr->c_type[id] = 'B';
			ip_prob_ptr->up_bound[id] = 1;
			ip_prob_ptr->low_bound[id] = 0;
			ip_prob_ptr->var_name[id] = (char*)malloc(sizeof(char)*1024);
				snprintf(       ip_prob_ptr->var_name[id],
								1024,
								"x_i%dj%d",
								i,j);
			id++;
		}
	}

	rval = CPXnewcols( ip_prob_ptr->env, ip_prob_ptr->lp, 
			nv, 
			ip_prob_ptr->cost, 
			ip_prob_ptr->low_bound,
			ip_prob_ptr->up_bound,
			ip_prob_ptr->c_type,
			ip_prob_ptr->var_name);
	if(rval)
		fprintf(stderr,"CPXnewcols returned errcode %d\n",rval);

	//Constraints part
        ip_prob_ptr->rhs = (double*)malloc(sizeof(double));
        ip_prob_ptr->sense = (char*)malloc(sizeof(char));
        ip_prob_ptr->rmatbeg = (int*)malloc(sizeof(int));
		ip_prob_ptr->nz = n+1;

        ip_prob_ptr->rmatind = (int*)malloc(sizeof(int)*nv);
        ip_prob_ptr->rmatval = (double*)malloc(sizeof(double)*nv);
		ip_prob_ptr->const_name = (char**)malloc(sizeof(char*));
		ip_prob_ptr->const_name[0] = (char*)malloc(sizeof(char)*1024);

	//We fill what we can 
	ip_prob_ptr->rmatbeg[0] = 0;

	//We generate and add each constraint to the model
	//Bin capacity constraints
	ip_prob_ptr->rhs[0] = 0;
	ip_prob_ptr->sense[0] = 'L';
	for( i = 0 ; i < n ; i++)
	{
		//Constraint name
	        snprintf(       ip_prob_ptr->const_name[0],
        	                1024,
                	        "capacity_bin_i%d",
                        	i);
		id=0;
		//variable y_i coefficient
	        ip_prob_ptr->rmatind[id] = id_y_i[i];
        	ip_prob_ptr->rmatval[id] =  -V;
		id++;
		//variables x_ij coefficients
		for( j = 0 ; j < n ; j++)
		{
		        ip_prob_ptr->rmatind[id] = id_x_ij[i][j];
        		ip_prob_ptr->rmatval[id] =  size[j];
			id++;
		}
		rval = CPXaddrows( ip_prob_ptr->env, ip_prob_ptr->lp, 
			0,//No new column
			1,//One new row
			n+1,//Number of nonzero coefficients
			ip_prob_ptr->rhs, 
			ip_prob_ptr->sense, 
			ip_prob_ptr->rmatbeg, 
			ip_prob_ptr->rmatind, 
			ip_prob_ptr->rmatval,
			NULL,//No new column
			ip_prob_ptr->const_name );
		if(rval)
			fprintf(stderr,"CPXaddrows returned errcode %d\n",rval);

	}


	/****************FILL HERE*******************/
	/*****Each item must be in exactly one bin***/
	/********************************************/

	ip_prob_ptr->rhs[0] = 1;
	ip_prob_ptr->sense[0] = 'E';
	for( i = 0 ; i < n ; i++)
		{
			//Constraint name
				snprintf(       ip_prob_ptr->const_name[0],
								1024,
								"number of bin used%d",
								i);
			id=0;
			//variable y_i coefficient
				ip_prob_ptr->rmatind[id] = id_y_i[i];
				ip_prob_ptr->rmatval[id] =  0;
			id++;
			//variables x_ij coefficients
			for( j = 0 ; j < n ; j++)
			{
					ip_prob_ptr->rmatind[id] = id_x_ij[j][i];
					ip_prob_ptr->rmatval[id] =  1;
				id++;
			}
			rval = CPXaddrows( ip_prob_ptr->env, ip_prob_ptr->lp, 
				0,//No new column
				1,//One new row
				n+1,//Number of nonzero coefficients
				ip_prob_ptr->rhs, 
				ip_prob_ptr->sense, 
				ip_prob_ptr->rmatbeg, 
				ip_prob_ptr->rmatind, 
				ip_prob_ptr->rmatval,
				NULL,//No new column
				ip_prob_ptr->const_name );
			if(rval)
				fprintf(stderr,"CPXaddrows returned errcode %d\n",rval);

		}

	//We write the problem for debugging purposes, can be commented afterwards
	rval = CPXwriteprob (ip_prob_ptr->env, ip_prob_ptr->lp, "bin_packing.lp", NULL);
	if(rval)
		fprintf(stderr,"CPXwriteprob returned errcode %d\n",rval);

	//We solve the model
	rval = CPXmipopt (ip_prob_ptr->env, ip_prob_ptr->lp);
	if(rval)
		fprintf(stderr,"CPXmipopt returned errcode %d\n",rval);

	rval = CPXsolwrite( ip_prob_ptr->env, ip_prob_ptr->lp, "bin_packing.sol" );
	if(rval)
		fprintf(stderr,"CPXsolwrite returned errcode %d\n",rval);

	//We get the objective value
	rval = CPXgetobjval( ip_prob_ptr->env, ip_prob_ptr->lp, &(ip_prob_ptr->objval) );
	if(rval)
		fprintf(stderr,"CPXgetobjval returned errcode %d\n",rval);

	//We get the best solution found 
	rval = CPXgetobjval( ip_prob_ptr->env, ip_prob_ptr->lp, &(ip_prob_ptr->objval) );
	rval = CPXgetx( ip_prob_ptr->env, ip_prob_ptr->lp, ip_prob_ptr->x, 0, nv-1 );
	if(rval)
		fprintf(stderr,"CPXgetx returned errcode %d\n",rval);

	//We display the solution
	double tolerance = 0.0001;
	int remaining;
	for( i = 0 ; i < n ; i++) 
	{
		id = id_y_i[i];
		if(ip_prob_ptr->x[id] <= 1-tolerance)
			continue;
		remaining = V;
		fprintf(stderr,"Bin #%d is used with volume %d and contains:\n",i,V);
		for( j = 0 ; j < n ; j++)
		{
			id = id_x_ij[i][j];
			if(ip_prob_ptr->x[id] <= 1-tolerance)
				continue;
			remaining -= size[j];
			fprintf(stderr,"\tItem #%d of volume %d (remaining: %d)\n",j,size[j],remaining);
		}
	}

	return rval;
}

void Heuri_tri(dataSet* data)
{
	int c = 0;
	for(int i = 0; i < data->n; i++)
	{
		for(int j = i+1; j < data->n; j++)
		{
			if(data->size[i] < data->size[j])
			{
				c = data->size[i];
				data->size[i] = data->size[j];
				data->size[j] = c;    
			}	
		}	
	}

} 

void Print_Bin(dataSet* data, int boites[], int res)
{
	for (int i = 0; i <= res; i++)
	{
		printf("la boites %d  de volumes %d (restant : %d)\n", i, data->V - boites[i], boites[i]);
	}	

}

//O(n*log(n))
int TP1_Heuristic_FF(dataSet* data)
{
	int ans = 0;

	int boites[data->n];
	for(int i = 0; i < data->n; i++)
	{
		boites[i] = data->V; 
	}  


	for(int i = 0; i < data->n; i++)
	{
		for(int j = 0; j < data->n; j++)
		{
			if(boites[j] >= data->size[i])
			{
				boites[j] = boites[j] - data->size[i];  
				break;
			} 
		}	
	} 
	for(int i = 0; i < data->n; i++)
	{
		if(boites[i] < data->V)
		{
			ans ++;
		} 
	}   
	printf("Le nombre de boites (FF) est de %d\n", ans);
	//Print_Bin(data, boites, ans);
	return ans;
}

//O(n*log(n))
int TP1_Heuristic_FFD(dataSet* data)
{
	int ans = 0;
	// Début du tri des valeurs par Poids décroissant
		
	Heuri_tri(data);

	// Fin du tri

	int boites[data->n];
	for(int i = 0; i < data->n; i++)
	{
		boites[i] = data->V; 
	}  


	for(int i = 0; i < data->n; i++)
	{
		for(int j = 0; j < data->n; j++)
		{
			if(boites[j] >= data->size[i])
			{
				boites[j] = boites[j] - data->size[i];  
				break;
			} 
		}	
	} 
	for(int i = 0; i < data->n; i++)
	{
		if(boites[i] < data->V)
		{
			ans ++;
		} 
	} 
	printf("Le nombre de boites (FFD) est de %d\n", ans);
	//Print_Bin(data,boites,ans);
	return ans;
}


//défault : pas de données sur le taux d'occupation des boites 
// avantages : O(n) et pas besoin de tableau en plus 
int TP1_Heuristic_NF(dataSet* data)
{
	int res = 0;
	int rem = data->V;


	for(int i = 0; i < data->n; i++)
	{
		if(rem < data->size[i])
		{
			res++;
			rem = data->V - data->size[i]; 
		} 
		else
		{
			rem -= data->size[i]; 
		} 


	} 
	printf("Le nombre de boites (NF) : %d\n", res);
	//Print_Bin();
	return res;
} 

//O(nlog(n))
int TP1_Heuristic_BF(dataSet* data)
{
	int res = 0;
	int bin[data->n];
	int index_min = 0;
	int poids_min = 0;

	for (int i = 0; i < data->n; i++)
	{
		index_min = 0;
		poids_min = data->V +1;

		for (int j = 0; j < res; j++)
		{
			if (bin[j] >= data->size[i] && bin[j] - data->size[i] < poids_min)    
			{
				index_min = j;
				poids_min = bin[j] - data->size[j];  
			} 
		} 

		if(poids_min == data->V+1)
		{
			bin[res] = data->V-data->size[i];
			res++;  
		} 
		else
		{
			bin[index_min] -= data->size[i];  
		} 
	}	
	printf("Le nombre de boites (BF) : %d\n", res);
	//Print_Bin(data,bin,res);
	return res;
}



int TP1_solve_heuristic(dataSet* data)
{
	clock_t temps_initial;
	clock_t temps_final;

	printf("\n");

	temps_initial = clock();
	TP1_Heuristic_NF(data);
	temps_final = clock();
	printf("temps d'execution de NF : %f ms\n",((double)(temps_final - temps_initial)*1000)/CLOCKS_PER_SEC);
	printf("\n");

	temps_initial = clock();
	TP1_Heuristic_FF(data);
	temps_final = clock();
	printf("temps d'execution de FF : %f ms\n",((double)(temps_final - temps_initial)*1000)/CLOCKS_PER_SEC);
	printf("\n");

	temps_initial = clock();
	TP1_Heuristic_BF(data);
	temps_final = clock();
	printf("temps d'execution de BF : %f ms\n",((double)(temps_final - temps_initial)*1000)/CLOCKS_PER_SEC);
	printf("\n");

	temps_initial = clock();
	TP1_Heuristic_FFD(data);	
	temps_final = clock();
	printf("temps d'execution de FFD : %f ms\n",((double)(temps_final - temps_initial)*1000)/CLOCKS_PER_SEC);
	printf("\n ");



	return 0;
}



